#!/bin/csh 

set MW_BUILDPATH=/usr/t1n1wall/build11
setenv MW_BUILDPATH $MW_BUILDPATH
setenv MW_ARCH `uname -m`

# ensure prerequisite tools are installed
if ( ! -x /usr/local/bin/bash ) then
	pkg install -y -g 'bash-4.*'
endif

# figure out if running as part of gitlab CI, if so get into the right place 
if ( $?CI_PROJECT_DIR == 1 ) then
	cd $CI_PROJECT_DIR
	echo "Running in CI enviroment"
	if ( -d "t1n1wall" ) then
	        rm -rf t1n1wall
	endif

	mkdir t1n1wall
	cd t1n1wall
	fetch https://gitlab.com/andywhite/t1n1wall/repository/freebsd11/archive.tar.gz
	tar -zxvf archive.tar.gz
	mv t1n1wall-* freebsd11
	cd freebsd11

else 
	# figure out if we're already running from within a repository
	if ( -d "t1n1wall" ) then
	        rm -rf t1n1wall
	endif

	mkdir t1n1wall
	cd t1n1wall
	fetch https://gitlab.com/andywhite/t1n1wall/repository/freebsd11/archive.tar.gz
	tar -zxvf archive.tar.gz
	mv t1n1wall-* freebsd11
	cd freebsd11

endif

echo "Creating build directory $MW_BUILDPATH."
mkdir -p $MW_BUILDPATH/freebsd11

echo "Exporting repository to $MW_BUILDPATH/freebsd11."
cp -fprv . $MW_BUILDPATH/freebsd11
/usr/local/bin/git ls-remote --tags https://gitlab.com/andywhite/t1n1wall.git | grep 2.11.1 | rev | cut -d b -f1 | rev| cut -d '^' -f1 | sort -n | head -n1 > $MW_BUILDPATH/freebsd11/svnrevision

echo "Changing directory to $MW_BUILDPATH/freebsd11/build/scripts"
cd $MW_BUILDPATH/freebsd11/build/scripts
chmod +x *.sh

echo "Updating ports to correct versions: 2016-10-16"

/usr/bin/svnlite checkout --depth empty svn://svn.freebsd.org/ports/head  $MW_BUILDPATH/tmp/ports/tree
cd $MW_BUILDPATH/tmp/ports/tree

/usr/bin/svnlite update -r '{2017-07-25}' --set-depth files Templates Tools net dns security sysutils devel GIDs UIDs Keywords
/usr/bin/svnlite update -r '{2017-07-25}' Mk net/isc-dhcp43-server net/isc-dhcp43-client net/mpd5/ net/dhcp6 sysutils/xmbmon
/usr/bin/svnlite update -r '{2017-07-25}' security/ipsec-tools devel/libtool net/openntpd
/usr/bin/svnlite update -r '{2017-07-25}' devel/gmake security/gnutls
/usr/bin/svnlite update -r '{2016-02-01}' net/wol net/openntpd 

cd $MW_BUILDPATH/freebsd11/build/scripts

echo 
echo "----- Build environment prepared -----"
echo "I will leave you in a bash shell now"
echo "To start the build, execute doall.sh or run 1makebuildenv.sh , then 2makebinaries.sh, then 3patchtools.sh etc"
setenv PS1 "t1n1wall-build# "
/usr/local/bin/bash
